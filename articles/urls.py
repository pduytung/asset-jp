from django.urls import path

from django.conf.urls.static import static
from django.conf import settings

from .views import (
    ArticleListView,

    ArticleListView1,

    ArticleUpdateView,
    ArticleDetailView,
    ArticleDeleteView,
    ArticleCreateView,  # new

    ArticleformView,
    
)

urlpatterns = [
    path('<int:pk>/edit/',
         ArticleUpdateView.as_view(), name='article_edit'),
    path('<int:pk>/',
         ArticleDetailView.as_view(), name='article_detail'),
    path('<int:pk>/delete/',
         ArticleDeleteView.as_view(), name='article_delete'),

    path('<int:pk>/form/',
         ArticleformView.as_view(), name='article_form'),

    path('new/', ArticleCreateView.as_view(), name='article_new'),
    
    path('', ArticleListView.as_view(), name='article_list'),

    path('save/', ArticleListView1.as_view(), name='article_save'),

   
    


   
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
